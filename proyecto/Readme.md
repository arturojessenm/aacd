Proyecto Módulo 2
================
23/4/2021

Responder las siguientes preguntas, y mandar un script de R que responda
las mismas. El método de entrega es via pull request al repositorio de
la clase: <https://gitlab.com/skalas/aacd>. Dentro de la carpeta
`proyecto/<nombre_del_equipo>`.

Se recomienda hacer en equipos de 2-3 personas. Hacer explícito el
nombre de los equipos en el archivo de entrega y en el pull request.

Con la base `flights` del paquete `nycflights13`. Responder las
siguientes preguntas:

  - ¿Cuál fue el peor día para viajar en 2013?
  - ¿Cuál es la peor aerolinea con respecto a retrasos?
  - ¿Cuál es la mejor aerolínea?
  - ¿Cuál es el destino más frecuente?
  - ¿De dónde venían la mayor cantidad de vuelos?
  - ¿Cuáles son las 10 rutas más transitadas?
  - ¿Qué día de la semana es el peor para viajar?

# Equipos

| Nombre                                | Correo                                | Equipo |
|:--------------------------------------|:-------------------------------------:|-------:|
| ELYS MELIZA FRIAS MELGAR              | ELYS.FRIAS@AB-INBEV.COM               |      1 |
| NUÑEZ LISSETTE ANNABEL                | LISSETTE.NUNEZ@AB-INBEV.COM           |      1 |
| ENRIQUE ANTONIO GINNARI NARANJO       | ENRIQUE.GINNARI@GMODELO.COM.MX        |      2 |
| GUILLERMO ANDRES DUARTE SUAREZ        | GUILLERMO.DUARTE@AB-INBEV.COM         |      2 |
| ARIANNA MARIEL JURADO AVILA           | ARIANNA.JURADO@AB-INBEV.COM           |      3 |
| MELANIE ANDUJAR PONCIANO              | MELANIE.ANDUJAR@AB-INBEV.COM          |      3 |
| ARTURO JESSEN MANRIQUE                | ARTURO.JESSEN@AB-INBEV.COM            |      4 |
| ERICK ALBERTO GUEVARA DAVILA          | ERICK.GUEVARA@AB-INBEV.COM            |      4 |
| ITZAYANA MONTESINOS AGUILAR           | ITZAYANA.MONTESINOS.A@AB-INBEV.COM    |      4 |
| DANIEL ROZO ISAZA                     | DANIEL.ROZOISAZA@AB-INBEV.COM         |      5 |
| MELISSA VILLADA HENAO                 | MELISSA.VILLADAHENAO@AB-INBEV.COM     |      5 |
| YULIANNY ESTEPHANY AYALA GARCIA       | YULIANNY.AYALA@AB-INBEV.COM           |      5 |
| ALEJANDRO KARLOS GERSHBERG RUBINSTEIN | ALEJANDRO.GERSHBERG@AB-INBEV.COM      |      6 |
| CINTHYA ANGELICA URBINA MARTINEZ      | CYNTHIA.URBINA@AB-INBEV.COM           |      6 |
| RODOLFO VARGAS BEZAURY                | RODOLFO.VARGAS@AB-INBEV.COM           |      6 |
| CARLOS EDUARDO SCHMIDT ALVARADO       | CARLOS.SCHMIDT@GMODELO.COM.MX         |      7 |
| CAROLINA GONZALEZ RANGEL              | CAROLINA.GONZALEZR@AB-INBEV.COM       |      7 |
| JOHAN DANIEL CONTRERAS RIVAS          | JOHAN.CONTRERAS@AB-INBEV.COM          |      7 |
| ANDRE CHAVEZ JIMENEZ                  | ANDRE.CHAVEZ@AB-INBEV.COM             |      8 |
| JAIME CALMET SAMANEZ                  | JAIME.CALMET@AB-INBEV.COM             |      8 |
| RAYZA RUTH MEDINA TERRONES            | RAYZA.MEDINA@AB-INBEV.COM             |      8 |
| JORGE ISRAEL LOMBARDERO JUAREZ        | JORGE.LOMBARDERO@GMODELO.COM.MX       |      9 |
| MARIANA MARTINEZ TRILLO               | MARIANA.MARTINEZ@AB-INBEV.COM         |      9 |
| EDGAR HERNANDEZ ANGELES               | EDGAR.HERNANDEZ@GMODELO.COM.MX        |     10 |
| RAUL MARTIN FLOR CHIRINOS             | RAUL.FLOR@AB-INBEV.COM                |     10 |
| YINETH FERNANDA QUIROGA HUERTAS       | FERNANDA.QUIROGA@AB-INBEV.COM         |     10 |
| HERMAN FRANCISCO HERNANDEZ CARDOZA    | HERMAN.HERNANDEZ@AB-INBEV.COM         |     11 |
| JOHN JAIRO LOPEZ RODRIGUEZ            | JOHNJAIRO.LOPEZRODRIGUEZ@AB-INBEV.COM |     11 |
| LADY MILENA MANCILLA LOPEZ            | LADY.MANCILLA@AB-INBEV.COM            |     11 |
| JUAN PABLO MOYANO ANTEPARA            | JUAN.MOYANO.A@AB-INBEV.COM            |     12 |
| JULIO ERNESTO CALDERON LOPEZ          | ERNESTO.CALDERON@AB-INBEV.COM         |     12 |
| WALTER JESUS PEREZ MORON              | WALTER.PEREZM@AB-INBEV.COM            |     12 |
| GIOVANNI ANDRES GUTIERREZ SANCHEZ     | GIOVANNI.GUTIERREZ@AB-INBEV.COM       |     13 |
| MARCO ANTONIO POUCHOULEN DEL CASTILLO | MARCO.POUCHOULEN@AB-INBEV.COM         |     13 |
| MARIA BELEN MOREIRA CASTRO            | MARIA.MOREIRAC@AB-INBEV.COM           |     13 |
